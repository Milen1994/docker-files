FROM ubuntu:16.04

LABEL MAINTAINER = Milen John Thomas

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get -y update && apt-get install -y \
    gcc \
    supervisor \
    php7.0-fpm php7.0 php7.0-soap php7.0-mbstring php7.0-curl php7.0-mysqli php7.0-zip \
    curl \
    nginx \
    locales && \
    sed -i -e 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' /etc/php/7.0/fpm/php.ini && \
    sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    ln -sf /dev/stdout /var/log/nginx/access.log  && \
    ln -sf /dev/stderr /var/log/nginx/error.log && \
    locale-gen && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

ENV LANG en_US.UTF-8  
ENV LANGUAGE en_US:en  
ENV LC_ALL en_US.UTF-8 

COPY docker-deps/nginx/nginx.conf /etc/nginx/nginx.conf
COPY docker-deps/nginx/mysite.conf /etc/nginx/conf.d/default.conf
COPY docker-deps/php-fpm/php-fpm.conf /etc/php/7.0/fpm/php-fpm.conf
COPY docker-deps/php-fpm/www.conf /etc/php/7.0/fpm/pool.d/www.conf
COPY docker-deps/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

COPY startup.sh /opt/startup.sh
RUN chmod 755 /opt/startup.sh && \
    chown -R www-data:www-data /var/www/

ADD app /var/www/
WORKDIR /var/www/

EXPOSE 80 9000
ENTRYPOINT ["/opt/startup.sh"]




 


